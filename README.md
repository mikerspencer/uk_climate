#Read me for UK climate plots#

##Basics#

Data taken from Met Office regional summaries.

Procressed to SQLite db.

Trends plotted.

##File order##
Sort of speaks for itself, but download.R first, then Writedb.R. Beyond that you'll want to source munging.R for functions and can source data.R to load data and then plots are in plots.R

Remember to change directory locations to reflect your own and install.packages("RSQLite")!

Latterly, plots.R now includes Scotland specific plots for the winter period.
